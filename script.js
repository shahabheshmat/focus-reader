// button event listener

const focusModeButton = document.getElementById('focus-button');
focusModeButton.addEventListener('click', initiateFocusMode);

function initiateFocusMode() {
    let focusArea = document.createElement('div');
    focusArea.id = 'focus-area';

    document.getElementById('overlay').classList.remove('hidden');

    const textArray = getParsedText();
    document.getElementsByTagName('body')[0].appendChild(focusArea);

    for (let i = 0; i < textArray.length; i++) {
        let sentence = document.createElement('div');
        sentence.innerHTML = textArray[i];
        sentence.className = 'micro-div';

        if (i === 0) {
            sentence.classList.add('focused');
        }

        document.getElementById('focus-area').appendChild(sentence);
    }
}

function getParsedText() {
    const pTagText = document.getElementById('sample-text').innerHTML;

    let entersRemoved = pTagText.replace(/(\r\n|\n|\r)/gm, '');
    let whiteSpaceRemoved = entersRemoved.replace(/\s+/g, ' ').trim();

    let result = whiteSpaceRemoved.split('.');

    result.pop();

    return result;
}

function focusPrevious() {
    const currentFocusedDiv = document.querySelector('.micro-div.focused');
    const previousSibling = currentFocusedDiv.previousElementSibling;
    if (previousSibling) {
        currentFocusedDiv.classList.remove('focused');
        previousSibling.classList.add('focused');
        centerFocusDiv();
    }
}

function focusNext() {
    const currentFocusedDiv = document.querySelector('.micro-div.focused');
    const nextSibling = currentFocusedDiv.nextElementSibling;
    if (nextSibling) {
        currentFocusedDiv.classList.remove('focused');
        nextSibling.classList.add('focused');
        centerFocusDiv();
    }
}

function centerFocusDiv() {
    const targetDiv = document.querySelector('.micro-div.focused');
    const focusArea = document.getElementById('focus-area');
    const focusAreaBoundingRect = focusArea.getBoundingClientRect();
    const targetDivBoundingRect = targetDiv.getBoundingClientRect();

    const targetDivCenter = targetDivBoundingRect.top + targetDivBoundingRect.height / 2;

    const targetScrollPosition = targetDivCenter - focusAreaBoundingRect.top;

    focusArea.scrollTop = targetDiv.offsetTop + targetDivBoundingRect.height / 2 - focusAreaBoundingRect.height / 2;
}

document.addEventListener('keydown', function (event) {
    event.preventDefault();

    switch (event.key) {
        case 'ArrowLeft':
            focusPrevious();
            break;
        case 'ArrowRight':
            focusNext();
            break;
        case 'ArrowDown':
            focusNext();
            break;
        case 'ArrowUp':
            focusPrevious();
            break;
    }
});

function clickOutside() {

    let divsToRemove = document.getElementsByClassName('micro-div');
    console.log(divsToRemove.length);
    for (let i = divsToRemove.length - 1; i >= 0; i--) {
        divsToRemove[i].remove();
    }
    
    document.getElementById('overlay').classList.add('hidden');
    document.getElementById('focus-area').remove();
};

//Handle click outside
document.getElementById('overlay').addEventListener('click', clickOutside);
